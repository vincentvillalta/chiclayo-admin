<?
require_once("includes/main.inc.php");


$start = intval($start);
$pagesize = intval($pagesize) == 0 ? $pagesize = DEF_PAGE_SIZE : $pagesize;
$columns = "select * ";
$sql = " from tbl_ride where ride_type = 'reservation'";
$sql .= " order by id";
$sql_count = "select count(*) " . $sql;
$sql .= " limit $start, $pagesize ";
$sql = $columns . $sql;
//echo $sql;
$result = db_query($sql);
$reccnt = db_scalar($sql_count);
?>
<script src="../js/validation.js"></script>
<link href="styles.css" rel="stylesheet" type="text/css">
<? include("top.inc.php"); ?>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td id="pageHead"><div id="txtPageHead">Reservaciones</div></td>
    </tr>
</table>

<table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td id="content" align="left">
            <strong class="msg"><div align="center"><?= display_sess_msg() ?></div></strong>
            <p align="center"></p>
            <p></p>
            <? if (mysql_num_rows($result) == 0) { ?>
                <div class="msg">No se encontraron registros.</div>
            <? } else {
                ?>
                <div align="right"> Mostrando registros: <?= $start + 1 ?> a   <?= ($reccnt < $start + $pagesize) ? ($reccnt - $start) : ($start + $pagesize) ?> <br> Total de registros: <?= $reccnt ?></div>
                <div align="left">Registros por página: <?= pagesize_dropdown('pagesize', $pagesize); ?></div>
                <p style="height:2px;"></p>
                <form method="post" name="form1" id="form1" onsubmit="">
                    <table width="100%"  border="0" cellpadding="0" cellspacing="1" class="tableList">
                        <tr>
                            <th width="5%">id </th>
                            <th width="5%">&nbsp;</th>
                            <th width="20%" nowrap="nowrap">Conductor</th>
                            <th width="20%" nowrap="nowrap">Pasajero</th>
                            <th width="10%" nowrap="nowrap">Fecha</th>
                            <th width="10%" nowrap="nowrap">Hora</th>
                            <th width="15%">Estado reserva</th>
                            <th width="15%">Estado carrera</th>
                        </tr>
                        <?
                        if ($start == 0) {
                            $cnt = 0;
                        } else {
                            $cnt = $start;
                        }

                        while ($line_raw = ms_stripslashes(mysql_fetch_array($result))) {
                            $cnt++;
                            $css = ($css == 'trOdd') ? 'trEven' : 'trOdd';
                            ?>
                            <tr class="<?= $css ?>">
                                <td align="center" valign="top"><?= $line_raw['id']; ?></td>
                                <td align="center" valign="top"><a href="change_reservation.php?id=<?= $line_raw['id']?>"><img src="images/icons/edit.png" alt="Edit" width="16" height="16" border="0" /></a></td>
                                <td align="left" valign="top"><? $driver = $line_raw['driver']; $result = mysql_query("SELECT `fullname` FROM tbl_user WHERE id = '$driver'") or die(mysql_error()); while($row = mysql_fetch_assoc($result)){ echo $row['fullname']; } ?><br />
                                    <a href='javascript:;' onClick="window.open('view_driver.php?did=<?= $line_raw['id']; ?>', '_blank', 'toolbar=no,menubar=no,scrollbars=yes,resizable=1,height=500,width=750');">Ficha completa</a></td>
                                <td align="left" valign="top"><? $passenger = $line_raw['passenger']; $result = mysql_query("SELECT `fullname` FROM tbl_user WHERE id = '$passenger'") or die(mysql_error()); while($row = mysql_fetch_assoc($result)){ echo $row['fullname']; } ?><br />
                                    <a href='javascript:;' onClick="window.open('view_passenger.php?pid=<?= $line_raw['id']; ?>', '_blank', 'toolbar=no,menubar=no,scrollbars=yes,resizable=1,height=500,width=750');">Perfil completo</a></td>
                                <td align="left" valign="top"><?= $line_raw['pickup_date']; ?></td>
                                <td align="left" valign="top"><?= $line_raw['pickup_time']; ?></td>
                                <td align="left" valign="top"><?= $line_raw['ride_status']; ?></td>
                                <td align="left" valign="top"><?= $line_raw['status']; ?></td>
                            </tr>
                        <?php } ?>
                    </table>
                </form>
            <?php } ?>
            <?php include("paging.inc.php"); ?>
        </td>
    </tr>
</table>
<?php include("bottom.inc.php"); ?>